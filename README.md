# remove credentials from a Gitlab project

This git repo illustrates how to rebase the whole commit history to e.g. get
rid of credentials stored in the first commit. This method was applied to this
repository as an example.

## the credential example

This recreates the lines that we will want to get rid of :

```
login: a_sample_login
password: *DELETED*
```

## how to proceed

Really purging old commits from a Gitlab server is a tricky task, as mentioned
in [Gitlab purge doc], because it involves deleting _all_ internal refs from
the server. There are 4 main steps :
  - download all git references from Gitlab by exporting the project
  - update the repository contents with `[git filter-repo]`
  - upload the modified contents
  - cleanup dangling references
When this is done, the Gitlab project will be purged of all references to those
old commits.

## a word of warning

Beware that any local copy of the repo made before this will still contain the
deleted commits. Also, users trying to pull from those old local copies will
end up in a mess.

## the exhaustive process

  - install [git filter-repo]
  - export the project from Gitlab following the [Gitlab export doc]: go to
    `Settings->Advanced`, click the `Export project button`, follow the link
    you receive by email to download the export
  - create a temporary folder and jump to it : `mkdir tmp && cd tmp`
  - unpack the exported archive : `tar xf path/to/XXXX-YY-ZZ...export.tar.gz`
  - clone the project bundle : `git clone --bare --mirror export.bundle`
  - copy all expressions that you want changed to `expressions.txt`

Example content :
```
a_sample_password==>*DELETED*
```
  - jump to the cloned repo : `cd project.git`
  - update the origin remote : `git remote set-url origin YOUR_REMOTE_ORIGIN`
  - run `git filter-repo --replace-text ../expressions.txt`
  - go to Gitlab `Settings->Repository->Protected` and unprotect any protected
    branches
  - upload the results with `git push origin --force 'refs/heads/*'`
  - remove dangling commits with `git push origin --force 'refs/replace/*'`
  - wait 30 minutes
  - in Gitlab `Settings->Repository`, run Repository Cleanup with the file
    `filter-repo/commit-map`

See more examples at [git-filter-repo examples]. Gitlab cleanup details are at
[Gitlab docs on repo cleanup].

[git filter-repo]: https://github.com/newren/git-filter-repo
[Gitlab export doc]: https://docs.gitlab.com/ee/user/project/settings/import_export.html#export-a-project-and-its-data 

[git-filter-repo examples]: https://htmlpreview.github.io/?https://github.com/newren/git-filter-repo/blob/docs/html/git-filter-repo.html#EXAMPLES
[Gitlab docs on repo cleanup]: https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html#repository-cleanup

